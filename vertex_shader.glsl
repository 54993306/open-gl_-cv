#version 330 core
layout (location = 0) in vec2 position;
out vec2 TexCoord;

void main()
{
    gl_Position = vec4(position, 0.0, 1.0);
    TexCoord = (position + 1.0) / 2.0;  // 将顶点坐标转换为纹理坐标
}
