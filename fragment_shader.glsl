#version 330 core
in vec2 TexCoord;
out vec4 FragColor;

uniform sampler2D textureSampler;  // 纹理采样器

void main()
{
    // 从纹理中采样颜色
    FragColor = texture(textureSampler, TexCoord);
}
