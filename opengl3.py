import glfw
from OpenGL.GL import *
import numpy as np

# 控制点
control_points = np.array([
    [-0.8, 0.0],
    [-0.5, 1.0],
    [0.5, 1.0],
    [0.8, 0.0]
])

# 初始化glfw
glfw.init()

# 创建窗口
window = glfw.create_window(800, 600, "OpenGL Window", None, None)
if not window:
    glfw.terminate()
    raise Exception("Failed to create GLFW window")

# 设置窗口为当前上下文
glfw.make_context_current(window)

# 计算抛物线上的点
# 计算抛物线上的点
t_values = np.linspace(0, 1, 100)[:, np.newaxis]  # 或者使用 t_values = np.linspace(0, 1, 100).reshape(-1, 1)
parabola_points = (1 - t_values)**2 * control_points[0] + 2 * (1 - t_values) * t_values * control_points[1] + t_values**2 * control_points[2]

# 主循环
while not glfw.window_should_close(window):
    # 渲染
    glClear(GL_COLOR_BUFFER_BIT)

    # 绘制抛物线
    glBegin(GL_LINE_STRIP)
    glColor3f(1.0, 1.0, 1.0)  # 设置颜色为白色
    for point in parabola_points:
        glVertex2f(point[0], point[1])
    glEnd()

    # 交换缓冲区并轮询事件
    glfw.swap_buffers(window)
    glfw.poll_events()

# 清理资源
glfw.terminate()
