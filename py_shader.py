import pyopengl

import shadertoy

with open("shader.glsl", "r") as f:
  shader_code = f.read()

shader = shadertoy.Shader(shader_code)

# 创建一个窗口
window = pyopengl.GLFWWindow(iResolution.x, iResolution.y, "My Shader")

# 渲染
while window.is_open:
  # 更新时间
  time = window.get_time()

  # 渲染图形
  shader.render(time)

  # 更新窗口
  window.update()