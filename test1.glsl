//Not the best but pretty good imo

#define s(x) smoothstep(0.15, 0.3, x * 1.1 - 0.1)

vec3 chromaKey(vec3 x, vec3 y){
	vec2 c = s(vec2(x.g - x.r * x.y, x.g));
    
    return mix(x, y, c.x * c.y);
}

vec3 getTexture(vec2 p){
	vec4 s = texture(iChannel0, p);
    return s.xyz * s.w;
}

vec3 getTexture1(vec2 p){
	vec4 s = texture(iChannel1, p);
    return s.xyz * s.w;
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 uv = fragCoord.xy / iResolution.xy;
    
    vec3 background = getTexture1(uv);
    
    vec3 color = getTexture(uv);
         color = chromaKey(color, background);
    
	fragColor = vec4(color,1.0);
}