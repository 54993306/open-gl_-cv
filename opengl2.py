import glfw
from OpenGL.GL import *
import numpy as np

# 定义球的运动轨迹点
trajectory_points = np.array([
    [0.0, 0.0],
    [0.2, 0.3],
    [0.5, 0.7],
    [0.8, 0.4],
    [1.0, 1.0]
])

# 初始化glfw
glfw.init()

# 创建窗口
window = glfw.create_window(800, 600, "OpenGL Window", None, None)
if not window:
    glfw.terminate()
    raise Exception("Failed to create GLFW window")

# 设置窗口为当前上下文
glfw.make_context_current(window)

# 主循环
while not glfw.window_should_close(window):
    # 渲染
    glClear(GL_COLOR_BUFFER_BIT)

    # 绘制运动轨迹线
    glBegin(GL_LINE_STRIP)
    glColor3f(1.0, 1.0, 1.0)  # 设置颜色为白色
    for point in trajectory_points:
        glVertex2f(point[0], point[1])
    glEnd()

    # 绘制箭头
    arrow_size = 0.05
    glBegin(GL_TRIANGLES)
    glColor3f(1.0, 0.0, 0.0)  # 设置颜色为红色
    last_point = trajectory_points[-1]
    glVertex2f(last_point[0], last_point[1])
    glVertex2f(last_point[0] - arrow_size, last_point[1] + arrow_size)
    glVertex2f(last_point[0] - arrow_size, last_point[1] - arrow_size)
    glEnd()

    # 交换缓冲区并轮询事件
    glfw.swap_buffers(window)
    glfw.poll_events()

# 清理资源
glfw.terminate()
