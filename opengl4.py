import glfw
from OpenGL.GL import *
from OpenGL.GLU import *
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import FancyArrowPatch
from matplotlib.animation import FuncAnimation
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import threading
import time

# 初始化 glfw
glfw.init()

# 创建窗口
window_width, window_height = 800, 600
glfw.window_hint(glfw.VISIBLE, True)
window = glfw.create_window(window_width, window_height, "OpenGL 窗口", None, None)
if not window:
    glfw.terminate()
    raise Exception("无法创建 GLFW 窗口")

glfw.make_context_current(window)

# 控制点
control_points = np.array([
    [-0.8, 0.0],
    [-0.5, 1.0],
    [0.5, 1.0],
    [0.8, 0.0]
])

# 计算抛物线上的点
t_values = np.linspace(0, 1, 8)[:, np.newaxis]
parabola_points = (1 - t_values)**2 * control_points[0] + 2 * (1 - t_values) * t_values * control_points[1] + t_values**2 * control_points[2]

# 初始化箭头位置和方向
arrow_position = parabola_points[-1]
arrow_direction = np.array([0.1 * np.sin(0), 0.1 * np.cos(0)])

# 创建箭头
arrow = FancyArrowPatch(arrow_position, arrow_position + arrow_direction, color='red', mutation_scale=15)
arrow.set_linewidth(2.0)

# 创建图和画布用于 Matplotlib 渲染
fig, ax = plt.subplots(figsize=(6, 6), dpi=100)
canvas = FigureCanvas(fig)

# 动画更新函数
def update(frame):
    global arrow_direction
    arrow_direction = np.array([0.1 * np.sin(frame / 10), 0.1 * np.cos(frame / 10)])
    arrow.set_positions((arrow_position[0], arrow_position[1]), (arrow_position[0] + arrow_direction[0], arrow_position[1] + arrow_direction[1]))

# 创建动画
animation = FuncAnimation(fig, update, frames=np.arange(0, 100), interval=50, blit=False)

# Matplotlib 绘制线程函数
def matplotlib_thread():
    while not glfw.window_should_close(window):
        ax.clear()
        ax.add_patch(arrow)
        ax.set_xlim(-1, 1)
        ax.set_ylim(-1, 1)
        canvas.draw()

        data = np.frombuffer(canvas.buffer_rgba(), dtype=np.uint8)
        data = data.reshape(canvas.get_width_height()[::-1] + (4,))
        glDrawPixels(data.shape[1], data.shape[0], GL_RGBA, GL_UNSIGNED_BYTE, data)

        time.sleep(0.05)  # 添加一个小延迟以控制渲染频率

# 启动 Matplotlib 绘制线程
matplotlib_rendering_thread = threading.Thread(target=matplotlib_thread)
matplotlib_rendering_thread.start()

# 主循环
while not glfw.window_should_close(window):
    glClear(GL_COLOR_BUFFER_BIT)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluOrtho2D(-1, 1, -1, 1)

    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    for point in parabola_points:
        glVertex2f(point[0], point[1])
    glEnd()

    glfw.swap_buffers(window)
    glfw.poll_events()
    # 使用 glfw.wait_events() 等待事件
    # glfw.wait_events()

   # 检测 ESC 键是否被按下
    if glfw.get_key(window, glfw.KEY_ESCAPE) == glfw.PRESS:
        close_window_flag = True

    # 如果关闭窗口标志为 True，则等待窗口关闭
    if close_window_flag and glfw.window_should_close(window):
        break

    time.sleep(0.5)

# 等待 Matplotlib 绘制线程结束
matplotlib_rendering_thread.join()
time.sleep(50)
# 清理资源
glfw.terminate()
